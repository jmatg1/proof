const apiUrl  ='http://vue-tests.dev.creonit.ru/api/'
///api/catalog?slug=diski

var app = new Vue({
    el: '#app',
    data: {
        cart: [
            {
                text: "Корзина",
                count: 0
            }
        ],
        categories: [
                {
                    id:0,
                    name: "Шины",
                    link: "shiny"
                },
                {
                    id:1,
                    name: "Диски",
                    link: "diski"
                }
            ],
        products:[],
        productsInCart:[],
        activeCategory:0
    },
    methods: {
        init: function () {
            app.selectCategory('shiny');
            app.getProductsInCart();
        },
        selectCategory: function (link,index) {
            if(app.activeCategory == index)
                return;
            app.activeCategory = index;

            $.ajax({
                url: apiUrl+"catalog/"+link,
                dataType: "json",
                success: function (json) {
                    app.products = json.items;
                    console.log(index);
                }
            });
        },
        addInCart: function (id) {
            $.ajax({
                url: apiUrl+"cart/product/"+id,
                dataType: "json",
                type: 'POST',
                method: 'POST',
                success: function (json) {
                    console.log(json);
                }
            });
            app.getProductsInCart();
            $('.cart').addClass('update-cart');
            setTimeout(function(){
                $('.cart').removeClass('update-cart');
            }, 500);
        },
        deleteInCart: function (id) {
            $.ajax({
                url: apiUrl+"cart/product/"+id,
                type: 'DELETE',
                success: function (json) {
                    console.log(json);
                }
            });
            app.getProductsInCart();
        },
        getProductsInCart: function () {
            $.ajax({
                url: apiUrl+"cart/list",
                dataType: "json",
                method: 'GET',
                success: function (json) {
                   app.cart[0].count = json.amount;
                   console.log(json);
                   app.productsInCart = json;
                }
            });
        }


    }
})
app.init()

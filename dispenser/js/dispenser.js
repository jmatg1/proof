var userWallet = [
    new money(1,10),
    new money(2,30),
    new money(5,20),
    new money(10,15)
];
var vmWallet = [
    new money(1,100),
    new money(2,100),
    new money(5,100),
    new money(10,100)
];
var products = [
    new product("Кофе", 18, 20, 0),
    new product("Чай", 13, 10, 1),
    new product("Кофе с молоком", 21, 20, 2),
    new product("Сок", 35, 15, 3)
];
var totalCost = 0;

function product(name, price, count,code) {
    this.name = name;
    this.price = price;
    this.count = count;
    this.code = code;
}
function setHTML(item) {
    return '<div>'
        + item.name + '(<span id="product-'+ item.code +'">' + item.count + '</span>)<br />'
        + item.price + ' р.'
        + '</div>'
        + '<div>'
        + '<button itemcode="' + item.code + '">Купить</button>'
        + '</div>';
}

for (var i = 0; i< products.length; i++){
    var contents = document.getElementById("panel_left");
    var div = document.createElement("div");
    div.className = "item";
    div.innerHTML = setHTML(products[i]);
    contents.appendChild(div);
}

function money(code, count) {
    this.code = code;
    this.count = count;
}

var doc_loaded = function () {
    document.getElementById("total_user").innerHTML = sumMoney(userWallet);
    document.getElementById("total_vm").innerHTML = sumMoney(vmWallet);
    document.getElementById("total_cost").innerHTML = totalCost;
    for (var i = 0; i < userWallet.length; i++) {
        document.getElementById("userWallet-"+userWallet[i].code).innerHTML = userWallet[i].count;
    }
    for (var i = 0; i < vmWallet.length; i++) {
        document.getElementById("vmWallet-"+vmWallet[i].code).innerHTML = vmWallet[i].count;
    }
    for (var i = 0; i < products.length; i++) {
        document.getElementById("product-"+products[i].code).innerHTML = products[i].count;
    }


};

function sumMoney(money) {
    var sum = 0;
    for (var i = 0; i <money.length; i++)
       sum+= money[i].code*money[i].count;
    return sum;
}
if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', doc_loaded, false);
} else if (document.attachEvent) {
    document.attachEvent('onreadystatechange', doc_loaded);
} else {
    window.onload = doc_loaded;
}


function returnCoin () {


    var returnCoinSet = [];

    var ten = Math.floor((totalCost % 1000) / 10);
    totalCost-=ten*10;


    var five = Math.floor((totalCost % 100) / 5);
    totalCost-=five*5;

    var two = Math.floor((totalCost % 100) / 2);
    totalCost-=two*2;

    var one = Math.floor((totalCost % 100));
    totalCost-=one;

    returnCoinSet.push(one);
    returnCoinSet.push(two);
    returnCoinSet.push(five);
    returnCoinSet.push(ten);
    console.log(ten+ ' ' +five+ ' ' +two+ ' '+one +'  ' +totalCost);
    for (var i = 0; i < userWallet.length; i++) {
        userWallet[i].count+= returnCoinSet[i]
        vmWallet[i].count -= returnCoinSet[i];
    }
};



var inputs = document.getElementById("panel_right");
inputs.onclick = function(e) {
    var target = e.target;
    var action = target.getAttribute('amount');
    if (!action)
        return;
    if (action == -1){
        returnCoin(totalCost);
        doc_loaded();
        return;
    }
    if  (userWallet[action].count === 0) {
        alert('Монеты ' + userWallet[action].code + ' Руб. закончились');
        return;
    }
    userWallet[action].count--;
    vmWallet[action].count++;
    totalCost+= userWallet[action].code;
    doc_loaded();
}
var inputs = document.getElementById("panel_left");
inputs.onclick = function(e) {
    var target = e.target;
    var action = target.getAttribute('itemcode');
    if (!action)
        return;
    if (products[action].price > totalCost){
        alert('Недостаточно средств');
        return;
    }
    if (products[action].count === 0){
        alert('Продукта нет');
        return;
    }
    products[action].count--;
    totalCost-=products[action].price;
    alert('Спасибо!');
    doc_loaded();
}


# Коммерческие и домашние проекты  

## Навигация
- [Коммерческие](#коммерческие)
   - [Интернет магазин Огород ](#интернет-магазин-огород )
- [Тестовые задания, обучение](#тестовые-задания-обучение)
   - [Vue.js](#vue-js)
   - [Dispenser](#dispenser)





## Коммерческие


### Интернет магазин Огород   
[Посмотреть](https://www.ogorodforgorod.ru/)  
Верстка и натяжка страниц. Сделал всё кроме главное то что ниже слайда, так как было уже сделано до меня. 
Также на странице списка продуктов не делал фильтр

Стек:

    jQuery, Yandex Maps, Bootstrap, Bitrix


## Тестовые задания, обучение

### Vue.js
[Посмотреть](https://www.ogorodforgorod.ru/)
[Исходный Код](https://gitlab.com/jmatg1/proof/-/tree/main/vue)  
Получить ответ от сервера и вывести 

Стек:

    Vue.js, jQuery

### Dispenser 
[Посмотреть](http://saroman.ru/portfolio/dispenser/)
[Посмотреть](https://gitlab.com/jmatg1/proof/-/tree/main/vue)
Автомат с напитками на чистом JavaScript

Стек:

    JavaScript
